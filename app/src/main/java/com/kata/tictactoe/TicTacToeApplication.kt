package com.kata.tictactoe

import android.app.Application
import com.kata.tictactoe.domain.model.Board
import com.kata.tictactoe.domain.model.Symbol
import com.kata.tictactoe.domain.usecase.PlaySymbolAt
import com.kata.tictactoe.feature.play.PlayViewModel
import org.koin.android.ext.koin.androidContext
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.core.context.startKoin
import org.koin.dsl.module

class TicTacToeApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@TicTacToeApplication)
            modules(modules)
        }
    }

}
