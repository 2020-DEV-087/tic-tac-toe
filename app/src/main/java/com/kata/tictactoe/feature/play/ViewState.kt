package com.kata.tictactoe.feature.play

import com.kata.tictactoe.domain.model.Board
import com.kata.tictactoe.domain.model.Symbol

sealed class ViewState {
    data class NewGame(val board: Board, val nextSymbolToPlay: Symbol) : ViewState()
    data class ShowBoard(val board: Board, val nextSymbolToPlay: Symbol) : ViewState()
    data class ShowWinner(val symbol: Symbol) : ViewState()
    object ShowDraw : ViewState()
}
