package com.kata.tictactoe.domain.usecase

import com.kata.tictactoe.domain.exception.InvalidPositionException
import com.kata.tictactoe.domain.exception.NotEmptySquareException
import com.kata.tictactoe.domain.model.Board
import com.kata.tictactoe.domain.model.BoardState
import com.kata.tictactoe.domain.model.Symbol.*
import org.junit.Assert.assertEquals
import org.junit.Test

class PlaySymbolAtTest {

    @Test
    fun `should detect win for 3 consecutive X on first row`() {
        val board = Board(3).apply {
            this[0, 0] = X
            this[0, 1] = X
        }
        val playSymbolAt = PlaySymbolAt(board)
        assertEquals(BoardState.Win(X), playSymbolAt(0, 2, X))
    }

    @Test
    fun `should detect win for 3 consecutive X on second row`() {
        val board = Board(3).apply {
            this[1, 0] = X
            this[1, 1] = X
        }
        val playSymbolAt = PlaySymbolAt(board)
        assertEquals(BoardState.Win(X), playSymbolAt(1, 2, X))
    }

    @Test
    fun `should detect win for 3 consecutive X on third row`() {
        val board = Board(3).apply {
            this[2, 0] = X
            this[2, 1] = X
        }
        val playSymbolAt = PlaySymbolAt(board)
        assertEquals(BoardState.Win(X), playSymbolAt(2, 2, X))
    }

    @Test
    fun `should detect win for 3 consecutive X on first column`() {
        val board = Board(3).apply {
            this[0, 0] = X
            this[1, 0] = X
        }
        val playSymbolAt = PlaySymbolAt(board)
        assertEquals(BoardState.Win(X), playSymbolAt(2, 0, X))
    }

    @Test
    fun `should detect win for 3 consecutive X on second column`() {
        val board = Board(3).apply {
            this[0, 1] = X
            this[1, 1] = X
        }
        val playSymbolAt = PlaySymbolAt(board)
        assertEquals(BoardState.Win(X), playSymbolAt(2, 1, X))
    }

    @Test
    fun `should detect win for 3 consecutive X on third column`() {
        val board = Board(3).apply {
            this[0, 2] = X
            this[1, 2] = X
        }
        val playSymbolAt = PlaySymbolAt(board)
        assertEquals(BoardState.Win(X), playSymbolAt(2, 2, X))
    }

    @Test
    fun `should detect win for 3 consecutive X on the NE-SO diagonal`() {
        val board = Board(3).apply {
            this[0, 0] = X
            this[1, 1] = X
        }
        val playSymbolAt = PlaySymbolAt(board)
        assertEquals(BoardState.Win(X), playSymbolAt(2, 2, X))
    }

    @Test
    fun `should detect win for 3 consecutive X on the SE-NO diagonal`() {
        val board = Board(3).apply {
            this[0, 2] = X
            this[1, 1] = X
        }
        val playSymbolAt = PlaySymbolAt(board)
        assertEquals(BoardState.Win(X), playSymbolAt(2, 0, X))
    }

    @Test
    fun `should detect win for 3 consecutive X on the SE-NO diagonal when playing center position`() {
        val board = Board(3).apply {
            this[0, 2] = X
            this[2, 0] = X
        }
        val playSymbolAt = PlaySymbolAt(board)
        assertEquals(BoardState.Win(X), playSymbolAt(1, 1, X))
    }

    @Test
    fun `should detect win for 3 consecutive X on the NE-SO diagonal when playing center position`() {
        val board = Board(3).apply {
            this[0, 0] = X
            this[2, 2] = X
        }
        val playSymbolAt = PlaySymbolAt(board)
        assertEquals(BoardState.Win(X), playSymbolAt(1, 1, X))
    }

    @Test
    fun `should detect a draw when last position is played`() {
        val board = Board(3).apply {
            this[0, 0] = X
            this[0, 1] = O
            this[1, 0] = O
            this[1, 1] = X
            this[1, 2] = O
            this[2, 0] = O
            this[2, 1] = X
            this[2, 2] = O
        }
        val playSymbolAt = PlaySymbolAt(board)
        assertEquals(BoardState.Draw, playSymbolAt(0, 2, X))
    }

    @Test
    fun `playing at an incorrect position should trigger an exception`() {
        val board = Board(3)
        val playSymbolAt = PlaySymbolAt(board)
        val result = playSymbolAt(4, 4, X)
        assert(result is BoardState.ForbiddenMove && result.throwable is InvalidPositionException)
    }

    @Test
    fun `playing at an occupied position should trigger an exception`() {
        val board = Board(3).apply {
            this[0, 0] = X
        }
        val playSymbolAt = PlaySymbolAt(board)
        val result = playSymbolAt(0, 0, X)
        assert(result is BoardState.ForbiddenMove && result.throwable is NotEmptySquareException)
    }
}
