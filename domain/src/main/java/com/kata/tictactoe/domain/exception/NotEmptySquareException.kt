package com.kata.tictactoe.domain.exception

class NotEmptySquareException : Exception()
