package com.kata.tictactoe.feature.play

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.core.view.children
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.kata.tictactoe.BOARD_SIZE
import com.kata.tictactoe.R
import com.kata.tictactoe.domain.model.Board
import com.kata.tictactoe.domain.model.Symbol
import kotlinx.android.synthetic.main.play_fragment.*
import org.koin.android.viewmodel.ext.android.viewModel

class PlayFragment : Fragment() {

    private val viewModel: PlayViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.play_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.state().observe(viewLifecycleOwner, Observer { state ->
            when (state) {
                is ViewState.NewGame -> initBoard(state.board, state.nextSymbolToPlay)
                is ViewState.ShowBoard -> displayBoard(state.board, state.nextSymbolToPlay)
                is ViewState.ShowDraw -> showDrawMessage()
                is ViewState.ShowWinner -> showWinnerMessage(state.symbol)
            }
        })
        playAgainButton.setOnClickListener { viewModel.onResetClicked() }
    }

    private fun initBoard(board: Board, nextSymbolToPlay: Symbol) {
        currentPlayerTextView.show()
        boardLayout.show()
        endGameMessage.hide()
        playAgainButton.hide()

        displayBoard(board, nextSymbolToPlay)
        boardLayout.columnCount = board.size
        boardLayout.removeAllViews()
        addCells(board)
    }

    private fun addCells(board: Board) {
        for (i in 0 until board.size * board.size) {
            val cell = LayoutInflater.from(requireContext())
                .inflate(R.layout.item_cell, boardLayout, false).apply {
                    setOnClickListener {
                        viewModel.onCellClicked(i / BOARD_SIZE, i % BOARD_SIZE)
                    }
                }
            boardLayout.addView(cell)
        }
    }

    private fun showWinnerMessage(symbol: Symbol) {
        currentPlayerTextView.hide()
        boardLayout.hide()
        endGameMessage.show()
        playAgainButton.show()
        endGameMessage.text = getString(R.string.winner_is, symbol.name)
    }

    private fun showDrawMessage() {
        currentPlayerTextView.hide()
        boardLayout.hide()
        endGameMessage.show()
        playAgainButton.show()
        endGameMessage.setText(R.string.draw)
    }

    private fun displayBoard(board: Board, nextSymbolToPlay: Symbol) {
        if (boardLayout.childCount == 0) {
            addCells(board)
        }
        currentPlayerTextView.text = getString(R.string.next_player, nextSymbolToPlay)
        boardLayout.children.forEachIndexed { index, view ->
            val resource = board[index / BOARD_SIZE, index % BOARD_SIZE].toDrawable()
            (view as? ImageView)?.setImageResource(resource)
        }
    }

    private fun Symbol.toDrawable() = when (this) {
        Symbol.Empty -> 0
        Symbol.X -> R.drawable.ic_cross
        Symbol.O -> R.drawable.ic_circle
    }

    private fun View.hide() {
        visibility = View.GONE
    }

    private fun View.show() {
        visibility = View.VISIBLE
    }

}
