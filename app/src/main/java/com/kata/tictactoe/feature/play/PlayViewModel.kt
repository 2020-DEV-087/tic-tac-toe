package com.kata.tictactoe.feature.play

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.kata.tictactoe.BOARD_SIZE
import com.kata.tictactoe.domain.model.Board
import com.kata.tictactoe.domain.model.BoardState
import com.kata.tictactoe.domain.model.Symbol
import com.kata.tictactoe.domain.usecase.PlaySymbolAt

class PlayViewModel(
    private val playSymbolAt: PlaySymbolAt
) : ViewModel() {
    private val _state = MutableLiveData<ViewState>()
    private var currentPlayer = Symbol.X

    init {
        initBoard()
    }

    fun state(): LiveData<ViewState> = _state

    fun onCellClicked(row: Int, column: Int) {
        when (val boardState = playSymbolAt(row, column, currentPlayer)) {
            is BoardState.Ongoing -> {
                currentPlayer = boardState.nextSymbolToPlay
                _state.value = ViewState.ShowBoard(boardState.board, currentPlayer)
            }
            is BoardState.Win -> _state.value = ViewState.ShowWinner(boardState.symbol)
            BoardState.Draw -> _state.value = ViewState.ShowDraw
        }
    }

    fun onResetClicked() {
        playSymbolAt.board = Board(BOARD_SIZE)
        initBoard()
    }

    private fun initBoard() {
        currentPlayer = Symbol.X
        _state.value = ViewState.NewGame(playSymbolAt.board, Symbol.X)
    }

}
