package com.kata.tictactoe.domain.model

sealed class BoardState {
    data class Ongoing(val board: Board, val nextSymbolToPlay: Symbol) : BoardState()
    data class Win(val symbol: Symbol) : BoardState()
    object Draw : BoardState()
    data class ForbiddenMove(val throwable: Throwable) : BoardState()
}
