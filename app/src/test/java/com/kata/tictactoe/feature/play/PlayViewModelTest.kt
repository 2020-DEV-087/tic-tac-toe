package com.kata.tictactoe.feature.play

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.kata.tictactoe.BOARD_SIZE
import com.kata.tictactoe.domain.model.Board
import com.kata.tictactoe.domain.model.Symbol
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.times
import com.nhaarman.mockitokotlin2.verify
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.koin.core.context.startKoin
import org.koin.core.context.stopKoin
import org.koin.test.KoinTest
import org.koin.test.inject

class PlayViewModelTest : KoinTest {
    @get:Rule
    val rule = InstantTaskExecutorRule()

    private val viewModel: PlayViewModel by inject()

    @Before
    fun setup() {
        startKoin {
            modules(com.kata.tictactoe.modules)
        }
    }

    @After
    fun tearDown() {
        stopKoin()
    }

    @Test
    fun `the board should be initialized with empty squares and X should play first`() {
        val observer = mock<Observer<ViewState>>()

        viewModel.state().observeForever(observer)
        val emptyBoard = Board(3)
        verify(observer, times(1)).onChanged(viewModel.state().value)
        assertEquals(ViewState.NewGame(emptyBoard, Symbol.X), viewModel.state().value)
    }

    @Test
    fun `the current player should alternate after a play`() {
        val observer = mock<Observer<ViewState>>()

        viewModel.state().observeForever(observer)
        viewModel.onCellClicked(0, 0)

        val expectedBoard = Board(3)
        expectedBoard[0, 0] = Symbol.X
        verify(observer).onChanged(viewModel.state().value)
        assertEquals(ViewState.ShowBoard(expectedBoard, Symbol.O), viewModel.state().value)
    }

    @Test
    fun `when a player wins a message should be shown`() {
        val observer = mock<Observer<ViewState>>()

        viewModel.state().observeForever(observer)
        viewModel.onCellClicked(0, 0)
        viewModel.onCellClicked(1, 0)
        viewModel.onCellClicked(0, 1)
        viewModel.onCellClicked(1, 1)
        viewModel.onCellClicked(0, 2)

        verify(observer).onChanged(viewModel.state().value)
        assertEquals(ViewState.ShowWinner(Symbol.X), viewModel.state().value)
    }

    @Test
    fun `when none of the players wins a message should be shown`() {
        val observer = mock<Observer<ViewState>>()

        viewModel.state().observeForever(observer)
        viewModel.onCellClicked(0, 0)
        viewModel.onCellClicked(1, 0)
        viewModel.onCellClicked(0, 1)
        viewModel.onCellClicked(0, 2)
        viewModel.onCellClicked(1, 2)
        viewModel.onCellClicked(1, 1)
        viewModel.onCellClicked(2, 0)
        viewModel.onCellClicked(2, 1)
        viewModel.onCellClicked(2, 2)

        verify(observer).onChanged(viewModel.state().value)
        assertEquals(ViewState.ShowDraw, viewModel.state().value)
    }

    @Test
    fun `a game can be restarted`() {
        val observer = mock<Observer<ViewState>>()

        viewModel.state().observeForever(observer)
        viewModel.onCellClicked(0, 0)
        viewModel.onCellClicked(1, 0)
        viewModel.onCellClicked(0, 1)

        viewModel.onResetClicked()

        assertEquals(ViewState.NewGame(Board(BOARD_SIZE), Symbol.X), viewModel.state().value)
    }
}