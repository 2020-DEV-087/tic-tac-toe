# Tic tac toe kata
![coverage](https://gitlab.com/2020-DEV-087/tic-tac-toe/badges/master/coverage.svg?job=test)

## Rules

The rules are described below :

- X always goes first.
- Players cannot play on a played position.
- Players alternate placing X’s and O’s on the board until either:
	- One player has three in a row, horizontally, vertically or diagonally
	- All nine squares are filled.
- If a player is able to draw three X’s or three O’s in a row, that player wins.
- If all nine squares are filled and neither player has three in a row, the game is a draw.

## Installation
### Build
#### Command line
- Clone this repo
- Navigate to the `tictactoe` folder
- Run `./gradlew assembleDebug`
- Run `adb install PATH_TO_APK`

#### Android Studio
- New > Project from Version Control...
- Paste the link to this repo
- After checking out the code, the IDE should suggest you to open the project.
- After an initial gradle sync, you can run the project like any other android project

#### Tests

Run unit tests:

`./gradlew test`

Run UI tests:

`./gradlew connectedAndroidTest`

## Process

### Git Branching Strategy
I followed the [GitHub flow](http://scottchacon.com/2011/08/31/github-flow.html) mainly for its simplicity and how well it works with CD

### Gitlab
I use this platform for the past 2 years. I picked it because I knew I could easily setup a CI.
The pipeline breaks into 2 stages:
- `check` : Runs [detekt](http://detekt.github.io) and unit tests
- `build` : Step that builds the app 

Every feature starts by branching `master`. 
Once the work is considered done, I create a merge request that I review to make sure I didn't miss something.
The pipeline would then validate my changes on every push by running the check stage.
Only a green pipeline can be merged to master.

### Merge requests
When a branch was ready to be merged, I would create a [merge request](https://gitlab.com/2020-DEV-087/tic-tac-toe/-/merge_requests?scope=all&utf8=✓&state=all) and review the code (even if I was working alone). It helps to switch from my "developer" hat (in the IDE) to my "reviewer" hat in the MR page.
When I would notice something missing that is not relevant in the MR, I would [create an issue](https://gitlab.com/2020-DEV-087/tic-tac-toe/-/issues?scope=all&utf8=✓&state=all) to make sure I don't forget it.
I also enabled [test reports within the merge request](https://docs.gitlab.com/ee/ci/junit_test_reports.html) to highlight the failing test in the pipeline overview.
With this, also comes the coverage report of the branch. Gitlab also [offers this](https://docs.gitlab.com/ee/ci/pipelines/settings.html#test-coverage-parsing) in the MR page.

## Architecture
### Clean architecture
The projects is split as follow :
- domain : Will contain domain specific classes (like the model, the use cases and the repositories interfaces). This is a pure Kotlin module. It won't contain any platform specific code.
- data: This module would be an android library. This is where we would add implementation of repositories interfaces. There was no platform specific implementation so I deleted that module.
- app: Android application module. It will depend on the domain and the data modules. This is where we'll glue everything together. It's also responsible of the UI layer.


![Clean dependency graph](doc/clean.png)

### Presentation Layer
I don't know who to call this pattern (I think the closest would be MVI). 
The idea is that the `ViewModel` just expose one `LiveData` on which an observer would subscribe and receive `ViewState` updates.

![MVI](doc/mvi.png)

## Useful links
* [MVI](https://www.raywenderlich.com/817602-mvi-architecture-for-android-tutorial-getting-started)
* [Clean Architecture](https://www.raywenderlich.com/3595916-clean-architecture-tutorial-for-android-getting-started)
* [Detekt](http://detekt.github.io)
* [GitLab CI/CD](https://docs.gitlab.com/ee/ci/yaml/)
