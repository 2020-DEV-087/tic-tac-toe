package com.kata.tictactoe.feature.play

import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.rule.ActivityTestRule
import com.kata.tictactoe.MainActivity
import com.kata.tictactoe.domain.model.Symbol
import com.kata.tictactoe.feature.play.robot.play
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class MainActivityTest {

    @get:Rule
    val activityRule = ActivityTestRule(MainActivity::class.java)

    @Test
    fun xShouldBeTheWinner() {
        play {
            play(0, 0)
            play(1, 0)
            play(0, 1)
            play(1, 1)
            play(0, 2)
        } then {
            symbolShouldBeTheWinner(activityRule.activity, Symbol.X)
            theBoardShouldNotBeVisible()
        }
    }

    @Test
    fun oShouldBeTheWinner() {
        play {
            play(0, 0)
            play(1, 0)
            play(0, 1)
            play(1, 1)
            play(2, 0)
            play(1, 2)
        } then {
            symbolShouldBeTheWinner(activityRule.activity, Symbol.O)
            theBoardShouldNotBeVisible()
        }
    }

    @Test
    fun shouldEndOnADraw() {
        play {
            play(0, 0)
            play(1, 0)
            play(0, 1)
            play(1, 1)
            play(1, 2)
            play(0, 2)
            play(2, 0)
            play(2, 1)
            play(2, 2)
        } then {
            shouldBeDraw()
            theBoardShouldNotBeVisible()
        }
    }


    @Test
    fun xShouldPlayFirst() {
        play { } then { shouldShowCurrentPlayerMessageFor(activityRule.activity, Symbol.X) }
    }

    @Test
    fun shouldAlternateNextPlayerMessage() {
        play { } then {
            shouldShowCurrentPlayerMessageFor(activityRule.activity, Symbol.X)
        }
        play { play(0, 0) } then {
            shouldShowCurrentPlayerMessageFor(activityRule.activity, Symbol.O)
        }
    }
}