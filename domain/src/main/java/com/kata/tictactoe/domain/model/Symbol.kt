package com.kata.tictactoe.domain.model

enum class Symbol {
    Empty,
    X,
    O
}
