package com.kata.tictactoe.domain.model

data class Board(
    val size: Int
) {
    private val grid = Array(size) { Array(size) { Symbol.Empty } }

    init {
        // Checks that all rows have the same amount of items
        val squareCount = grid.first().size
        check(grid.all { it.size == squareCount }) { "All rows should have the same amount of squares" }
    }

    fun getRow(row: Int): Array<Symbol> {
        return grid[row]
    }

    fun getColumn(column: Int): Array<Symbol> {
        return grid.map { it[column] }.toTypedArray()
    }

    fun getDiagonal(): Array<Symbol> {
        return grid.mapIndexed { index, columns ->
            columns[index]
        }.toTypedArray()
    }

    fun getAntiDiagonal(): Array<Symbol> {
        return grid.mapIndexed { index, columns ->
            columns[columns.size - 1 - index]
        }.toTypedArray()
    }

    operator fun get(row: Int, column: Int): Symbol {
        return grid[row][column]
    }

    operator fun set(row: Int, column: Int, symbol: Symbol) {
        grid[row][column] = symbol
    }

    fun hasEmptySquares() = grid.any { it.contains(Symbol.Empty) }

}
