package com.kata.tictactoe

import com.kata.tictactoe.domain.model.Board
import com.kata.tictactoe.domain.usecase.PlaySymbolAt
import com.kata.tictactoe.feature.play.PlayViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val modules = module {
    viewModel { PlayViewModel(get()) }
    factory { PlaySymbolAt(get()) }
    factory {
        createEmptyBoard()
    }
}

private fun createEmptyBoard() = Board(BOARD_SIZE)

const val BOARD_SIZE = 3
