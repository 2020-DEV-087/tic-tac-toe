package com.kata.tictactoe.feature.play.robot

import android.content.Context
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import com.kata.tictactoe.R
import com.kata.tictactoe.domain.model.Symbol
import org.hamcrest.Matchers.not
import org.hamcrest.core.AllOf.allOf

fun play(func: PlayRobot.() -> Unit) = PlayRobot().apply { func() }

class PlayRobot {

    fun play(row: Int, column: Int) {
        onView(
            allOf(
                withParent(withId(R.id.boardLayout)),
                withParentIndex(row * 3 + column)
            )
        ).perform(
            click()
        )
    }

    infix fun then(func: ResultRobot.() -> Unit): ResultRobot {
        return ResultRobot().apply { func() }
    }

}

class ResultRobot {
    fun symbolShouldBeTheWinner(context: Context, symbol: Symbol) {
        val expectedText = context.getString(R.string.winner_is, symbol)
        onView(withId(R.id.endGameMessage)).check(
            matches(
                allOf(
                    isDisplayed(),
                    withText(expectedText)
                )
            )
        )
    }

    fun theBoardShouldNotBeVisible() {
        onView(withId(R.id.boardLayout)).check(matches(not(isDisplayed())))
    }

    fun shouldBeDraw() {
        onView(withId(R.id.endGameMessage)).check(
            matches(
                allOf(
                    isDisplayed(),
                    withText(R.string.draw)
                )
            )
        )
    }

    fun shouldShowCurrentPlayerMessageFor(context: Context, symbol: Symbol) {
        val expectedText = context.getString(R.string.next_player, symbol)
        onView(withId(R.id.currentPlayerTextView)).check(
            matches(
                allOf(
                    isDisplayed(),
                    withText(expectedText)
                )
            )
        )
    }
}
