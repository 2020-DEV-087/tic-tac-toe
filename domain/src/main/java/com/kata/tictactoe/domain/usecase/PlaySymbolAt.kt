package com.kata.tictactoe.domain.usecase

import com.kata.tictactoe.domain.exception.InvalidPositionException
import com.kata.tictactoe.domain.exception.NotEmptySquareException
import com.kata.tictactoe.domain.model.Board
import com.kata.tictactoe.domain.model.BoardState
import com.kata.tictactoe.domain.model.Symbol

class PlaySymbolAt(
    var board: Board
) {

    /**
     * Plays a symbol at a given position and returns a BoardState based on the last move
     */
    operator fun invoke(row: Int, column: Int, symbol: Symbol) = play(row, column, symbol)

    private fun play(row: Int, column: Int, symbol: Symbol): BoardState {
        return when {
            isInvalidPosition(row, column) -> BoardState.ForbiddenMove(InvalidPositionException())
            isSquareNotEmpty(row, column) -> BoardState.ForbiddenMove(NotEmptySquareException())
            else -> {
                board[row, column] = symbol
                when {
                    checkRowWin(row, symbol) || checkColumnWin(column, symbol) || checkDiagonalWin(
                        symbol
                    ) -> BoardState.Win(symbol)
                    !board.hasEmptySquares() -> BoardState.Draw
                    else -> BoardState.Ongoing(board, nextPlayer(symbol))
                }
            }
        }
    }

    private fun nextPlayer(symbol: Symbol) = if (symbol == Symbol.X) Symbol.O else Symbol.X

    private fun checkDiagonalWin(symbol: Symbol) =
        board.getDiagonal().all { it == symbol } // NE to SO diagonal
                || board.getAntiDiagonal().all { it == symbol } // SE to NO diagonal

    private fun checkColumnWin(column: Int, symbol: Symbol) =
        board.getColumn(column).all { it == symbol }

    private fun checkRowWin(row: Int, symbol: Symbol) = board.getRow(row).all { it == symbol }

    private fun isSquareNotEmpty(row: Int, column: Int) = board[row, column] != Symbol.Empty

    private fun isInvalidPosition(row: Int, column: Int) =
        row >= board.size || column >= board.size

}
