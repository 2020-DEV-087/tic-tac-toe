package com.kata.tictactoe

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.kata.tictactoe.feature.play.PlayFragment

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, PlayFragment())
                .commitNow()
        }
    }
}
