package com.kata.tictactoe.domain.exception

class InvalidPositionException : Exception()
